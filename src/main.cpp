#include <iostream>

#include "NotUtil.h"

int main() {
    std::cout << "Hello, " << NotUtil::nameTheWorld(true) << "! I am happy" << std::endl;
    std::cout << "Hello, " << NotUtil::nameTheWorld(false) << "! I am sad" << std::endl;
    return 0;
}
